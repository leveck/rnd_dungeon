#!/bin/bash
cat <<EOF
    ░█▀▄░█▀█░█▀▄░░░█▀▄░█░█░█▀█░█▀▀░█▀▀░█▀█░█▀█
    ░█▀▄░█░█░█░█░░░█░█░█░█░█░█░█░█░█▀▀░█░█░█░█
    ░▀░▀░▀░▀░▀▀░░░░▀▀░░▀▀▀░▀░▀░▀▀▀░▀▀▀░▀▀▀░▀░▀

EOF
printf " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv\n"
for (( row=1; row<=30; row++ )); do
  for (( col=1; col<=50; col++ )); do
    if [ $row = 1 ] || [ $row = 30 ] || [ $col = 1 ] || [ $col = 50 ]; then
     if [ $row -eq 1 -a $col -le 5 -a ! $col -eq 1 -o $row -eq 30 -a $col -ge 46 -a ! $col -eq 50 ]; then
       rowcontent="$rowcontent "
     else
       rowcontent="$rowcontent█"
     fi
    else
      x=$(( ( RANDOM %20 ) + 1 ))
      if [ $x -le 14 ]; then
        rowcontent="$rowcontent "
      else
        rowcontent="$rowcontent▓"
      fi
    fi
  done
  if [ $row -le 9 ]; then
    rp="0$row"
  else
    rp="$row"
  fi
  printf "$rowcontent$rp\n"
  rowcontent=""
done
