#!/bin/bash
# dungeon2.sh
# Copyright (C) 2020 leveck <leveck@leveck.us>

roomfile="rooms"
cat <<EOF
    ░█▀▄░█▀█░█▀▄░░░█▀▄░█░█░█▀█░█▀▀░█▀▀░█▀█░█▀█
    ░█▀▄░█░█░█░█░░░█░█░█░█░█░█░█░█░█▀▀░█░█░█░█
    ░▀░▀░▀░▀░▀▀░░░░▀▀░░▀▀▀░▀░▀░▀▀▀░▀▀▀░▀▀▀░▀░▀

               D=door S=hidden door
     Tip: play like pacman: doors on left wrap
        to doors on right (and vice versa)
	      Reload for new dungeon

EOF
for i in {1..6}
do
paste <(shuf -n 1 $roomfile | par -w 10) \
      <(shuf -n 1 $roomfile | par -w 10) \
      <(shuf -n 1 $roomfile | par -w 10) \
      <(shuf -n 1 $roomfile | par -w 10) \
      <(shuf -n 1 $roomfile | par -w 10) \
      | sed 's/\t//g;s/DD/D /g;s/X/█/g;s/E/ /g'
done
