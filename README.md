# RND DUNGEON

A work in progress dungeon generator. See it live at [RPoD Gopherhole] or via [RPoD Gopher=>HTTP Proxy].

## FILES

First version is just purely random blocks.
```
   dungeon.sh
   ascidungeon.sh
```

Current version uses the rooms data file which contains 52 rooms. You can add more.
```
   dungeon2.sh
   asciidungeon2.sh
   rooms
```
Rooms are 10x5 characters. In the rooms file, a room is 50 characters all on one line, read left to right, and the code chops it down to 5 rows of 10 columns. X is a wall, E is empty. Other characters (such as D for door, and S for secret door) remain in the program output. You can take advantage of this to add things like monsters or treasure, for example.

## HOW A DATA STRING BECOMES A ROOM

```
$ echo EEEEEXXXEEEEEXXXEXEEEXXXEEESEEEEXXXXXXEEXEEEEEEEEX | par -w 10 | sed 's/X/█/g;s/E/ /g'
     ███
   ███ █
 ███   S
  ██████
█        █
```
The ```paste``` command is used to stack rooms next to each other.

## SCREENCAP

![Screencap](screencap.png)

[RPoD Gopherhole]: gopher://1436.ninja:70/1/menu/04GAMES
[RPoD Gopher=>HTTP Proxy]: https://leveck.us/1436.ninja:70/1/menu/04GAMES
